#include"tocka.h"
#include<iostream>
#include<cmath>

using namespace std;

//METODE ZA TOCKU
void Tocka::setTocka(double a, double b, double c){
    duzina = a;
    sirina = b;
    visina = c;
}
double Tocka::getDuzina() const{
    return duzina;
}
double Tocka::getSirina() const{
    return sirina;
}
double Tocka::getVisina() const{
    return visina;
}
double Tocka::udaljenost2D(Tocka t2){
    return sqrt(pow((sirina - t2.sirina), 2) + (pow((duzina - t2.duzina), 2)));
}
double Tocka::udaljenost3D(Tocka t2){
    return sqrt(pow((sirina - t2.sirina), 2) + pow((duzina - t2.duzina), 2) + pow((visina - t2.visina), 2));
}
void Tocka::pseudoRandom(int maxSirina, int maxDuzina, int maxVisina){
    duzina = (double) (rand() % ( maxDuzina + 1 ));
    sirina = (double) (rand() % ( maxSirina + 1 ));
    visina = (double) (rand() % ( maxVisina + 1 ));
}
//METODE ZA ORUZJE
void Oruzje::shoot(){
    --brMetaka;
}
void Oruzje::reload(){
    brMetaka = 6;
}
double Oruzje::stanjeMetaka() const{
    return brMetaka;
}
void Oruzje::draw(Tocka t1){
    pozicijaOruzja = t1;
    reload();
}
double Oruzje::getSirina() const{
    return pozicijaOruzja.getSirina();
}
double Oruzje::getDuzina() const{
    return pozicijaOruzja.getDuzina();
}
double Oruzje::getVisina() const{
    return pozicijaOruzja.getVisina();
}

//METODE ZA METU
void Meta::setMeta(Tocka t2, double sirina, double visina){
    lijevaTocka = t2;
    sirinaMete = sirina;
    visinaMete = visina;
}
//Metode za dohvacanje sirine i visine mete od koordinate tocke
double Meta::getSirinaMete() const{
    return sirinaMete;
}
double Meta::getVisinaMete() const{
    return visinaMete;
}
//Metode za dohvacanje koordinata lijeve tocke
double Meta::getSirina() const{
    return lijevaTocka.getSirina();
}
double Meta::getDuzina() const{
    return lijevaTocka.getDuzina();
}
double Meta::getVisina() const{
    return lijevaTocka.getVisina();
}