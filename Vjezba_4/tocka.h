class Tocka{

    double duzina;
    double sirina;
    double visina;
    
    public:
    void setTocka(double a = 0, double b = 0, double c = 0);
    double getDuzina() const;
    double getSirina() const;
    double getVisina() const;
    double udaljenost2D(Tocka t2);
    double udaljenost3D(Tocka t2);
    void pseudoRandom(int maxSirina, int maxDuzina, int maxVisina);
};

class Oruzje{

    Tocka pozicijaOruzja;
    int brMetaka;
    int brMetakaSanzer;

    public:
    void shoot();
    void reload();
    double stanjeMetaka() const;
    void draw(Tocka t1);
    double getSirina() const;
    double getVisina() const;
    double getDuzina() const;
};

class Meta{

    Tocka lijevaTocka;
    double sirinaMete;
    double visinaMete;
    bool pogodak;

    public:
    void setMeta(Tocka t2, double sirina, double visina);
    double getSirinaMete() const;
    double getVisinaMete() const;
    double getSirina() const;
    double getVisina() const;
    double getDuzina() const;
    bool setPogodak(bool);
};