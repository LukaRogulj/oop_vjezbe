#include <iostream>
#include "Zbroj.h"
#include "Stack.h"
#include <string>


int main()
{
	Zbroj<int> zb;
	std::cout << zb.add(3, 7) << std::endl;
	Zbroj<char> zb1;
	std::cout << zb1.add('e', 'f') << std::endl;

	Stack<std::string> st;
	std::cout << st.size() << std::endl;
	st.push("l");
	st.push("u");
	st.push("k");
	st.push("a");
	std::cout << st.pop() << std::endl;
	std::cout << st.cap() << std::endl;
	std::cout << st.pop() << std::endl;
	std::cout << st.size() << std::endl;
	st.~Stack();
}
