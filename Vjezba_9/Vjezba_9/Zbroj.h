#pragma once
#include <iostream>
#include <cctype>

template <typename T>
class Zbroj
{
public:
	Zbroj() { std::cout << "normalna" << std::endl; }
	~Zbroj() {}
	T add(T a, T b) {
		return a + b;
	}
};

template <>
class Zbroj<char> {

public:
	Zbroj() { std::cout << "spec klasa" << std::endl; }
	~Zbroj() {}
	char add(char a, char b) {
		if (isdigit(a) && isdigit(b)) {
			return ((a - '0') + (b - '0') + '0');
		}
		return (a - '0') + (b - '0');
	}
};

