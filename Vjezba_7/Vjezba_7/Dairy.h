#pragma once
#include "Food.h"
class Dairy :
	public Food
{
public:
	Dairy() {
		usedAsDish = 0;
		usedAsIngredient = 0;
	}
	virtual ~Dairy() {};
	virtual double getDish() = 0;
	virtual double getIngredient() = 0;
	virtual void incDish() = 0;
	virtual void incIngredient() = 0;

protected:
	double usedAsDish;
	double usedAsIngredient;

};

