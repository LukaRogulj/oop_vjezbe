#include "Milk.h"



Milk::Milk()
{
}


Milk::~Milk()
{
}

double Milk::getIngredient() {
	return usedAsIngredient;
}

double Milk::getDish() {
	return usedAsDish;
}

void Milk::incDish() {
	usedAsDish++;
}

void Milk::incIngredient() {
	usedAsIngredient++;
}