#pragma once
#include "Meat.h"
class Meso :
	public Meat
{
public:
	Meso();
	~Meso();
	double getIngredient();
	double getDish();
	void incDish();
	void incIngredient();

	friend ostream& operator<<(ostream& output, Meso& x) {
		output << "Potrosnja mesa kao jelo " << x.getDish() << std::endl << "Potrosnja mesa kao dio drugog jela " << x.getIngredient() << std::endl;
		return output;
	}
};

