#pragma once
#include "Dairy.h"
class Cheese :
	public Dairy
{
public:
	Cheese();
	~Cheese();
	double getIngredient();
	double getDish();
	void incDish();
	void incIngredient();

	friend ostream& operator<<(ostream& output, Cheese& x) {
		output << "Potrosnja sira kao jelo " << x.getDish() << std::endl << "Potrosnja sira kao dio drugog jela " << x.getIngredient() << std::endl;
		return output;
	}
};

