#pragma once
#include "Meat.h"
class Ham :
	public Meat
{
public:
	Ham();
	~Ham();
	double getIngredient();
	double getDish();
	void incDish();
	void incIngredient();

	friend ostream& operator<<(ostream& output, Ham& x) {
		output << "Potrosnja sunke kao jelo " << x.getDish() << std::endl << "Potrosnja sunke kao dio drugog jela " << x.getIngredient() << std::endl;
		return output;
	}
};

