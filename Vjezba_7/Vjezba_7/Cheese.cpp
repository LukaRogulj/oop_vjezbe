#include "Cheese.h"



Cheese::Cheese()
{
}


Cheese::~Cheese()
{
}

double Cheese::getIngredient() {
	return usedAsIngredient;
}

double Cheese::getDish() {
	return usedAsDish;
}

void Cheese::incDish() {
	usedAsDish++;
}

void Cheese::incIngredient() {
	usedAsIngredient++;
}
