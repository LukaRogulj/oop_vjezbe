#pragma once
#include "Cake.h"
class Madjarica :
	public Cake
{
public:
	Madjarica();
	~Madjarica();
	double getDish();
	void incDish();

	friend ostream& operator<<(ostream& output, Madjarica &x) {
		output << "Potrosnja madjarice kao jelo " << x.getDish() << std::endl;
		return output;
	}
};

