#include "Meso.h"





double Meso::getIngredient() {
	return usedAsIngredient;
}

double Meso::getDish() {
	return usedAsDish;
}

void Meso::incDish() {
	usedAsDish++;
}

void Meso::incIngredient() {
	usedAsIngredient++;
}