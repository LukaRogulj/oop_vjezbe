#pragma once
#include "Dairy.h"
class Milk :
	public Dairy
{
public:
	Milk();
	~Milk();
	double getIngredient();
	double getDish();
	void incDish();
	void incIngredient();

	friend ostream& operator<<(ostream& output, Milk& x) {
		output << "Potrosnja mlijeka kao jelo " << x.getDish() << std::endl << "Potrosnja mlijeka kao dio drugog jela " << x.getIngredient() << std::endl;
		return output;
	}
};

