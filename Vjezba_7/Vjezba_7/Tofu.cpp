#include "Tofu.h"



double Tofu::getSide() {
	return usedAsSide;
}

double Tofu::getDish() {
	return usedAsDish;
}

void Tofu::incDish() {
	usedAsDish++;
}

void Tofu::incSide() {
	usedAsSide++;
}
