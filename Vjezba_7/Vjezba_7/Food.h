#pragma once
#include<iostream>
#include<string>
#include"UsedFood.h"

using namespace std;

class Food
{
	string type;
	string name;
	float waterContent;
	string exprDate;
	float proteinAmount;
	float fatAmount;
	float carbAmount;
	float dailyUsage;
	UsedFood* consumption;
	int currentIndex = 0;

public:
	Food() {};
	Food(string type, string name, float waterContent, float proteinAmount, float fatAmount,
		float carbAmount, float dailyUsage, string exprDate);
	Food(const Food& f2);
	~Food();
	string getType() const;
	string getName() const;
	float getWaterContent() const;
	float getProteinAmount() const;
	float getFatAmount() const;
	float getCarbAmount() const;
	int getDailyUsage() const;
	string getExprDate() const;
	void incDailyUsage();
	void decDailyUsage();
	bool isCurrYear(int year);
	void addConsuption(UsedFood);
	int getCurrentIndex() const;
	void incCurrentIndex();
	float checkConsumption();
	void printObj();


	int allocateConsump(string exprDate) const;

	/* Point(const Point &p2) {x = p2.x; y = p2.y; }  */
};

