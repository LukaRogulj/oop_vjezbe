#pragma once
#include "Cake.h"
class Torta :
	public Cake
{
public:
	Torta();
	~Torta();
	double getDish();
	void incDish();

	friend ostream& operator<<(ostream& output, Torta& x) {
		output << "Potrosnja torte kao jelo " << x.getDish() << std::endl;
		return output;
	}
};

