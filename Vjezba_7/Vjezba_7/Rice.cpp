#include "Rice.h"


double Rice::getSide() {
	return usedAsSide;
}

double Rice::getDish() {
	return usedAsDish;
}

void Rice::incDish() {
	usedAsDish++;
}

void Rice::incSide() {
	usedAsSide++;
}