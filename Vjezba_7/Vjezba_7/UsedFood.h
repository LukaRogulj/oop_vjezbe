#pragma once
#include<iostream>
#include<string>

using namespace std;
class UsedFood
{
	int year;
	int month;
	float kg;

public:
	UsedFood(int year, int month, float kg);
	int getYear() const;
	int getMonth() const;
	float getKg() const;
	UsedFood();
};

