#pragma once
#include "Meat.h"
class Prossciuto :
	public Meat
{
public:
	Prossciuto();
	~Prossciuto();
	double getIngredient();
	double getDish();
	void incDish();
	void incIngredient();

	friend ostream& operator<<(ostream& output, Prossciuto& x) {
		output << "Potrosnja prsuta kao jelo " << x.getDish() << std::endl << "Potrosnja prsuta kao dio drugog jela " << x.getIngredient() << std::endl;
		return output;
	}
};

