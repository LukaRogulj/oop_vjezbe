#pragma once
#include "Vege.h"
class Hummus :
	public Vege
{
public:
	Hummus();
	~Hummus();
	double getSide();
	double getDish();
	void incDish();
	void incSide();

	friend ostream& operator<<(ostream& output, Hummus& x) {
		output << "Potrosnja humusa kao jelo " << x.getDish() << std::endl << "Potrosnja humusa kao priloga" << x.getSide() << std::endl;
		return output;
	}
};

