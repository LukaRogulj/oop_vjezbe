#pragma once
#include "Cake.h"
class Krempita :
	public Cake
{
public:
	Krempita();
	~Krempita();
	double getDish();
	void incDish();

	friend ostream& operator<<(ostream& output, Krempita& x) {
		output << "Potrosnja krempite kao jelo " << x.getDish() << std::endl;
		return output;
	}
};

