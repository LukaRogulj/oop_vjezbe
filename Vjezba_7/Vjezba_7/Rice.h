#pragma once
#include "Vege.h"
class Rice :
	public Vege
{
public:
	Rice();
	~Rice();
	double getSide();
	double getDish();
	void incDish();
	void incSide();

	friend ostream& operator<<(ostream& output, Rice& x) {
		output << "Potrosnja rize kao jelo " << x.getDish() << std::endl << "Potrosnja rize kao priloga" << x.getSide() << std::endl;
		return output;
	}
};

