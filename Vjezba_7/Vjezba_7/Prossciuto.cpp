#include "Prossciuto.h"



double Prossciuto::getIngredient() {
	return usedAsIngredient;
}

double Prossciuto::getDish() {
	return usedAsDish;
}

void Prossciuto::incDish() {
	usedAsDish++;
}

void Prossciuto::incIngredient() {
	usedAsIngredient++;
}