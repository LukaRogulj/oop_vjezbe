#include "Hummus.h"



double Hummus::getSide() {
	return usedAsSide;
}

double Hummus::getDish() {
	return usedAsDish;
}

void Hummus::incDish() {
	usedAsDish++;
}

void Hummus::incSide() {
	usedAsSide++;
}