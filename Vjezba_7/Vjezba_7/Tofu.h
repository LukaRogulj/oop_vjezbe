#pragma once
#include "Vege.h"
class Tofu :
	public Vege
{
public:
	Tofu();
	~Tofu();
	double getSide();
	double getDish();
	void incDish();
	void incSide();

	friend ostream& operator<<(ostream& output, Tofu& x) {
		output << "Potrosnja tofua kao jelo " << x.getDish() << std::endl << "Potrosnja tofua kao priloga" << x.getSide() << std::endl;
		return output;
	}
};

