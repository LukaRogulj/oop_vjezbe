#pragma once
#include "Dairy.h"
class Yogurth :
	public Dairy
{
public:
	Yogurth();
	~Yogurth();
	double getIngredient();
	double getDish();
	void incDish();
	void incIngredient();

	friend ostream& operator<<(ostream& output, Yogurth& x) {
		output << "Potrosnja jogurta kao jelo " << x.getDish() << std::endl << "Potrosnja jogurta kao dio drugog jela " << x.getIngredient() << std::endl;
		return output;
	}
};

