#include "Ham.h"



double Ham::getIngredient() {
	return usedAsIngredient;
}

double Ham::getDish() {
	return usedAsDish;
}

void Ham::incDish() {
	usedAsDish++;
}

void Ham::incIngredient() {
	usedAsIngredient++;
}