#pragma once
#include "Food.h"
class Vege :
	public Food
{
	Vege() {
		usedAsDish = 0;
		usedAsSide = 0;
	}
	virtual ~Vege() {};
	virtual double getDish() = 0;
	virtual double getIngredient() = 0;
	virtual void incDish() = 0;
	virtual void incIngredient() = 0;

protected:
	double usedAsDish;
	double usedAsSide;

};

