#include "UsedFood.h"


float UsedFood::getKg() const {
	return kg;
}
int UsedFood::getMonth() const {
	return month;
}
int UsedFood::getYear() const {
	return year;
}
UsedFood::UsedFood(int year, int month, float kg) {
	this->year = year;
	this->month = month;
	this->kg = kg;
}

UsedFood::UsedFood() {}


