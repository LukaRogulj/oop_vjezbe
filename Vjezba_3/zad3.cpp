#include<iostream>
#include<string>
#include<cctype>
#include<vector>



using namespace std;

void correctString(string& str){

    for(string::iterator it = str.begin(); it != str.end(); ++it){
        if((*it == ' ' && ispunct(*(it + 1)) || (*it == ' ' && *(it + 1) == ' '))){
            str.erase(it);
            --it;
        }
        if(*it == ',' && *(it + 1) != ' '){
            str.insert((it + 1), 1, ' ');
        }
    }


}

int main(){
    string str = "Ja bih ,     ako  ikako  mogu   ,     ovu rečenicu     napisala ispravno .";
    correctString(str);
    cout << str << endl;
}

