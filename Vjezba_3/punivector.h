#include<iostream>
#include<vector>
#include<ctime>

using namespace std;

int randomNumber(int a, int b){
    return a + (rand() % ( b - a + 1 ));
}

vector <int> inputVector(int a = 5, int b = 60, int n = 5, bool generateRandom = true){

    if(a < 0 || a > 100){
        a = 0;
    }
    if(b < 0 || b > 100){
        b =  100;
    }

    vector <int> input;

    if(!generateRandom){
        for(int i = 0; i < n; ++i){
            input.push_back(a + i);
        }
    }else{
        for(int i = 0; i < n; ++i){
            input.push_back(randomNumber(a, b));
        }
    }
    return input;
}

void printVector(vector <int> input){
    for(int i = 0; i < input.size(); ++i){
        cout << input[i] << endl;
    }
}

