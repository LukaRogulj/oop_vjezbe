#include<iostream>
#include<iomanip>

int main(){

    int niz[] = {1 ,4, 8, 6 ,4, 4, 2, 9, 1, 7, 0, 2};
    int len = sizeof(niz)/sizeof(int);

    int nizDuplikati[9] = {0};

    for (int i = 0; i < len; i++){

        for(int j = i + 1; j < len; j++){

            if(niz[i] == niz[j]){
                nizDuplikati[niz[i]] += 1;
            }
        }
    }

    for (int i = 0; i < 9; i++){
        std::cout << nizDuplikati[i] << std::endl;
    }

}