#include<iostream>
#include"vec.h"
#include<cmath>

using namespace std;
using namespace OOP;

 Vec3::Vec3(double x, double y, double z) {
     Vec3::incCount();
     _x = x;
     _y = y;
     _z = z;
 }

 Vec3::Vec3() {
     Vec3::incCount();
     _x = _y = _z = 0;
 }

 Vec3::Vec3(const Vec3& v) {
     _x = v._x;
     _y = v._y;
     _z = v._z;
 }

 double Vec3::getX() const {
     return _x;
 }
 double Vec3::getY() const {
     return _y;
 }
 double Vec3::getZ() const {
     return _z;
 }

 int Vec3::getTotalCount() {
     return getCount();
 }
 void Vec3::incCount() {
     getCount()++;
 }

Vec3 Vec3::operator+(const Vec3& v) {
    return Vec3((_x + v._x), (_y + v._y), (_z + v._z));
}

Vec3 Vec3::operator-(const Vec3& v) {
    return Vec3((_x - v._x), (_y - v._y), (_z - v._z));
}

bool Vec3::operator==(const Vec3& v) {
    return(_x == v._x && _y == v._y && _z == v._z);
}

bool Vec3::operator!=(const Vec3& v) {
    return(_x != v._x && _y != v._y && _z != v._z);
}

Vec3 Vec3::operator=(const Vec3& v) {
    return Vec3((_x = v._x), (_y = v._y), (_z = v._z));
}

Vec3 Vec3::operator+=(const Vec3& v) {
    return Vec3((_x += v._x), (_y += v._y), (_z += v._z));
}

Vec3 Vec3::operator-=(const Vec3& v) {
    return Vec3((_x -= v._x), (_y -= v._y), (_z -= v._z));
}

Vec3 Vec3::operator*=(const Vec3& v) {
    return Vec3((_x *= v._x), (_y *= v._y), (_z *= v._z));
}

Vec3 Vec3::operator/=(const Vec3& v) {
    return Vec3((_x /= v._x), (_y /= v._y), (_z /= v._z));
}

Vec3 Vec3::operator*=(double s) {    
     return Vec3((_x *= s), (_y *= s), (_z *= s));
}

Vec3 Vec3::operator/=(double s) {
    return Vec3((_x /= s), (_y /= s), (_z /= s));
}

double Vec3::operator[](unsigned int i) const {
    if(i > 2) exit(1);
    if(i == 0) return _x;
    if(i == 1) return _y;
    if(i == 2) return _z; 
}

double Vec3::operator*(Vec3 v) {
    return _x * v._x + _y * v._y + _z * v._z;
}

void Vec3::normalize() {
    _x = _x / (sqrt(pow(_x, 2) + pow(_y, 2) + pow(_z, 2)));
    _y = _y / (sqrt(pow(_x, 2) + pow(_y, 2) + pow(_z, 2)));
    _z = _z / (sqrt(pow(_x, 2) + pow(_y, 2) + pow(_z, 2)));
}

// /*
// The Vector3D class: represents a 3 dimensional vector. Only has 3 variables: x, y, and z for displacement.
// */
 
// /* class Vector3D {
//  public:
 
// /*Variables:
//  I didn't make x, y, and z private, as the programmer should already know the inner workings of a vector,
// and would figure that there would be an x, y, and z variables. Plus, I hate the look of setX(int x) and getX() :D .
// */
//   double x, y, z;
 
// /*Constructors:
//  Vector3D v: x, y, and z are all set to 0.
//  Vector3D v2(1, 2, 3): x, y, and z are set to 1, 2, and 3, respectivly.
//  Vector3D v3(Vector3D v4): v3.x, v3.y, and v3.z are all set to v4.x, v4.y, and v4.z, respectivly.
// */
//   Vector3D() {
//    x = y = z = 0;
//   }
 
//   Vector3D(double a, double b, double c) {
//    x = a;
//    y = b;
//    z = c;
//   }
 
//   Vector3D(const Vector3D &v) {
//    x = v.x;
//    y = v.y;
//    z = v.z;
//   }
 
//   Vector3D operator+(Vector3D v);
//   Vector3D operator-(Vector3D v);
//   Vector3D operator*(double w);
//   Vector3D operator/(double w);
//   Vector3D operator=(Vector3D v);
 
// /*Other functions:
//  void negate(): negate the vector - x, y, and z are set to -x, -y, and -z, respectivly
//  double length(): returns the length of the vector.
//  void normalize(): normalize the vector, vector / length of vector
// */
//   void negate();
//   double length();
//   void normalize();
// };
 
// Vector3D Vector3D::operator+(Vector3D v) {
//  return Vector3D((x + v.x), (y + v.y), (z + v.z));
// }
 
// Vector3D Vector3D::operator-(Vector3D v) {
//  return Vector3D((x - v.x), (y - v.y), (z - v.z));
// }
 
// Vector3D Vector3D::operator*(double w) {
//  return Vector3D((x * w), (y * w), (z * w));
// }
 
// Vector3D Vector3D::operator/(double w) {
//  return Vector3D((x / w), (y / w), (z / w));
// }
 
// Vector3D Vector3D::operator=(Vector3D v) {
//  x = v.x;
//  y = v.y;
//  z = v.z;
//  return *this;
// }
 
// void Vector3D::negate() {
//  x = -x;
//  y = -y;
//  z = -z;
// }
 
// double Vector3D::length() {
//  return sqrt((x * x) + (y * y) + (z * z));
// }
 
// void Vector3D::normalize() {
//  x /= length();
//  y /= length();
//  z /= length();
// } 