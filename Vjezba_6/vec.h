#include <iostream>
using namespace std;

namespace OOP {

    class Vec3 {
    double _x, _y, _z;
    static int& getCount() {
        static int count = 0;
        return count;
    }

    public:
    Vec3(double, double, double);
    Vec3();
    Vec3(const Vec3& v);
    int getTotalCount();
    void incCount();
    double getX() const;
    double getY() const;
    double getZ() const;
    Vec3 operator+(const Vec3&);
    Vec3 operator-(const Vec3&);
    bool operator==(const Vec3&);
    bool operator!=(const Vec3&);
    Vec3 operator=(const Vec3&);
    Vec3 operator+=(const Vec3&);
    Vec3 operator-=(const Vec3&);
    Vec3 operator*=(const Vec3&);
    Vec3 operator/=(const Vec3&);
    Vec3 operator*=(double);
    Vec3 operator/=(double);
    double operator[](unsigned int) const;
    double operator*(Vec3);
    void normalize();
    /* friend ostream& operator<<(ostream &output, const Vec3& v);
    friend istream &operator>>( istream  &input, Vec3 &v ); */
    friend ostream& operator<<(ostream &output, const Vec3& v) {
        output << "X : " << v._x << " Y : " << v._y << " z : " << v._z;
        return output;            
    }        
    friend istream &operator>>( istream  &input, Vec3 &v ) { 
        input >> v._x >> v._y >> v._z;
        return input;    
    }
};
    /* ostream& operator<<(ostream &output, const Vec3& v) {
        output << "X : " << v._x << " Y : " << v._y << " z : " << v._z;
        return output;            
    }        
    istream &operator>>( istream  &input, Vec3 &v ) { 
        input >> v._x >> v._y >> v._z;
        return input;    
    } */
}



