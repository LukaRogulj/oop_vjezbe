#include<iostream>
#include"food.h"
#include<ctime>
#include<algorithm>

using namespace std;

Food::Food(const Food &f2){
    type = f2.type;
    name = f2.name;
    waterContent =f2.waterContent;
    exprDate = f2.exprDate;
    proteinAmount = f2.proteinAmount;
    fatAmount = f2.fatAmount;
    carbAmount = f2.carbAmount;
    dailyUsage = f2.dailyUsage;

    int size = sizeof(f2.consumption)/sizeof(UsedFood);
    consumption =  new UsedFood[allocateConsump(exprDate)];
    copy(f2.consumption,f2.consumption + size, consumption);

}


Food::Food(string typeA, string nameA, float waterContentA, float proteinAmountA,
         float fatAmountA, float carbAmountA, int dailyUsageA, string exprDateA){

    type = typeA;
    name = nameA;
    waterContent = waterContentA;
    exprDate = exprDateA;
    proteinAmount = proteinAmountA;
    fatAmount = fatAmountA;
    carbAmount = carbAmountA;
    dailyUsage = dailyUsageA;

    consumption = new UsedFood[allocateConsump(exprDate)];
}

int Food::allocateConsump(string date){
    int year = stoi(date.substr(date.length() - 4));

    time_t now = time(0);
    tm *ltm = localtime(&now);
    int currYear = 1970 + ltm->tm_year;

    int toAllocate = currYear + year;

    if(toAllocate == 0){
        return 12;
    }

    return toAllocate * 12;

}



/* <ctyme>
int trenGod(){
    time_h t = time(0);
    struct* tm now = localtime(&t);
    now->tm-year + 1;
    now->mon + 1;
} */