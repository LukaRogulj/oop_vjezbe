#include<string>

using namespace std;

class Food{
    string type;
    string name;
    float waterContent;
    string exprDate;
    float proteinAmount;
    float fatAmount;
    float carbAmount;
    int dailyUsage;
    UsedFood *consumption;
    
    public:
    Food(string type, string name, float waterContent, float proteinAmount, float fatAmount,
         float carbAmount, int dailyUsage, string exprDate);
    Food(const Food &f2);

    int allocateConsump(string exprDate);

    /* Point(const Point &p2) {x = p2.x; y = p2.y; }  */
};

class UsedFood{
    int yeat;
    int month;
    float kg;
};