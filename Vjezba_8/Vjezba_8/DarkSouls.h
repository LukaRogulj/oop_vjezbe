#pragma once
#include "Rpg.h"
#include <string>
#include <vector>

namespace OSS {
	class DarkSouls :
		public Rpg
	{
	public:
		DarkSouls();
		~DarkSouls();
		std::string get_type() const;
		std::vector <std::string> platforms() const;
		void insert(std::string);
	protected:
		std::vector <std::string> _platforms;


	};
}

