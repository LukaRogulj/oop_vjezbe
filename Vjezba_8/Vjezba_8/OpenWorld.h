#pragma once
#include "VideoGames.h"
#include <vector>

namespace OSS {
	class OpenWorld :
		virtual public VideoGames
	{
	public:
		OpenWorld();
		~OpenWorld();
		//virtual std::vector<std::string> platforms();
		std::string get_type();

	protected:
		std::string type = "OpenWorld";
	};
}

