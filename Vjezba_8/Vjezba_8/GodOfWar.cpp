#include "GodOfWar.h"
#include <string>
#include <vector>
using namespace OSS;

GodOfWar::GodOfWar() {};

GodOfWar::~GodOfWar() {};

void GodOfWar::insert(std::string s) {
	_platforms.push_back(s);
}

std::string GodOfWar::get_type() const {
	return type;
}

std::vector<std::string> GodOfWar::platforms() const {
	return _platforms;
}