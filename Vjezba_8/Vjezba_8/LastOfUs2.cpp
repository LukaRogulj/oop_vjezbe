#include "LastOfUs2.h"
#include <string>
#include <vector>
using namespace OSS;


LastOfUs2::LastOfUs2() {};

LastOfUs2::~LastOfUs2() {};

void LastOfUs2::insert(std::string s) {
	_platforms.push_back(s);
}

std::string LastOfUs2::get_type() const {
	return type;
}

std::vector<std::string> LastOfUs2::platforms() const {
	return _platforms;
}