#pragma once
#include "Action.h"
#include <string>
#include <vector>

namespace OSS {
	class GodOfWar :
		public Action
	{
	public:
		GodOfWar();
		~GodOfWar();
		std::string get_type() const;
		std::vector <std::string> platforms() const override;
		void insert(std::string);
	protected:
		std::vector <std::string> _platforms;

	};
}

