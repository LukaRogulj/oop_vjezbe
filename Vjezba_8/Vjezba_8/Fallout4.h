#pragma once
#include "Rpg.h"

namespace OSS {
	class Fallout4 :
		public Rpg
	{
	public:
		Fallout4();
		~Fallout4();
		std::string get_type() const;
		std::vector <std::string> platforms() const;
		void insert(std::string);
	protected:
		std::vector <std::string> _platforms;

	};
}

