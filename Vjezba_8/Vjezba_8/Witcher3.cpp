#include "Witcher3.h"
#include <string>
#include <vector>
using namespace OSS;


Witcher3::~Witcher3() {};

void Witcher3::insert(std::string s) {
	_platforms.push_back(s);
}

std::string Witcher3::get_type() {
	return _type;
}

std::vector<std::string> Witcher3::platforms() const {
	return _platforms;
}