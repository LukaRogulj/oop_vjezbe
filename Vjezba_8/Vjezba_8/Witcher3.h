#pragma once
#include "OpenWorld.h"
#include "Rpg.h"

namespace OSS {
	class Witcher3 :
		virtual public Rpg,
		virtual public OpenWorld
	{
		
	public:
		std::string _type;
		Witcher3() { _type = "OpenWorld Rpg"; }
		~Witcher3();
		std::string get_type();
		std::vector <std::string> platforms() const;
		void insert(std::string);
	private:
		std::vector <std::string> _platforms;

	};
}

