#pragma once
#include "VideoGames.h"
#include <string>
namespace OSS {
	class Action :
		public VideoGames
	{
	public:
		Action();
		~Action();
		std::string get_type();
	protected:
		std::string type = "Action";
	};
}

