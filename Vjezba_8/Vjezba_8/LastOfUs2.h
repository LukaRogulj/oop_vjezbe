#pragma once
#include "Action.h"
#include <string>
#include <vector>

namespace OSS {
	class LastOfUs2 :
		public Action
	{
	public:
		LastOfUs2();
		~LastOfUs2();
		std::string get_type() const;
		std::vector <std::string> platforms() const;
		void insert(std::string);
	protected:
		std::vector <std::string> _platforms;

	};
}

