#pragma once
#include <string>
#include <vector>

namespace OSS {
	class VideoGames
	{
	public:
		VideoGames();
		virtual std::string get_type() = 0;
		virtual ~VideoGames() {};
		virtual std::vector<std::string> platforms() const = 0;
		virtual void insert(std::string) = 0;
	};
}

