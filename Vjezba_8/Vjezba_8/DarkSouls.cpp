#include "DarkSouls.h"
#include <string>
#include <vector>
using namespace OSS;

DarkSouls::DarkSouls() {};

DarkSouls::~DarkSouls() {};

void DarkSouls::insert(std::string s) {
	_platforms.push_back(s);
}

std::string DarkSouls::get_type() const{
	return type;
}

std::vector<std::string> DarkSouls::platforms() const{
	return _platforms;
}
