#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "VideoGames.h"
#include "Action.h"
#include "Rpg.h"
#include "OpenWorld.h"
#include "GodOfWar.h"
#include "DarkSouls.h"
#include "Fallout4.h"
#include "LastOfUs2.h"
#include "Witcher3.h"
#include "Counter.h"

using namespace OSS;

std::vector<std::string> getPlatforms(std::string& s) {
	std::vector <std::string> platforms;
	
		if (s.find("PC") != std::string::npos)
			platforms.push_back("PC");
		if (s.find("XBOX") != std::string::npos)
			platforms.push_back("XBOX");
		if (s.find("PS4") != std::string::npos)
			platforms.push_back("PS4");
		return platforms;

}

int main() {
	VideoGames* games[] = { new GodOfWar, new Fallout4, new DarkSouls, new LastOfUs2, new Witcher3 };
	std::ifstream file("videogames.txt");
	std::string line, gameName, gamePlatform, temp;
	std::string delimiter = "#";
	std::vector <std::string> playablePlatforms;
	std::vector <std::string> playableGames;
	int pos;

	while (getline(file, line)) {
		pos = line.find(delimiter);
		gameName = line.substr(0, pos);
		gamePlatform = line.substr(pos + 1, line.size());
		playablePlatforms = getPlatforms(gamePlatform);

		if (gameName.size() > 0) {
			if (gameName == "GodOfWar") {
				for (int i = 0; i < playablePlatforms.size(); ++i) {
					games[0]->insert(playablePlatforms[i]);
				}
			}
			else if (gameName == "Fallout4") {
				for (int i = 0; i < playablePlatforms.size(); ++i) {
					games[1]->insert(playablePlatforms[i]);
				}
			}
			else if (gameName == "DarkSouls") {
				for (int i = 0; i < playablePlatforms.size(); ++i) {
					games[2]->insert(playablePlatforms[i]);
				}
			}
			else if (gameName == "LastOfUs2") {
				for (int i = 0; i < playablePlatforms.size(); ++i) {
					games[3]->insert(playablePlatforms[i]);
				}
			}
		}
	}
	Counter counter;
	for (int i = 0; i < 4; ++i) {
		counter.platformCounter(games[i]);
	}
	std::cout << "naj crossplatform je: " << counter.mostCommon() << std::endl;


	//file.close();
	//delete games;
}

