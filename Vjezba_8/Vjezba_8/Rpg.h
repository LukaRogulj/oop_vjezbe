#pragma once
#include "VideoGames.h"
#include <string>
#include <vector>

namespace OSS {
	class Rpg :
		virtual public VideoGames
	{
	public:
		Rpg();
		~Rpg();
		std::string get_type();
	protected:
		std::string type;
	};
}

