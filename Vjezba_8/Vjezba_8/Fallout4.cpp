#include "Fallout4.h"
#include <string>
#include <vector>
using namespace OSS;

Fallout4::Fallout4() {};

Fallout4::~Fallout4() {};

void Fallout4::insert(std::string s) {
	_platforms.push_back(s);
}

std::string Fallout4::get_type() const
{
	return type;
}

std::vector<std::string> Fallout4::platforms() const {
	return _platforms;
}
