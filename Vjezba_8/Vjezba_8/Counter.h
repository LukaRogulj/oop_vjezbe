#pragma once
#include "Videogames.h"

namespace OSS {

	class Counter
	{
	private:
		int _xbox;
		int _ps4;
		int _pc;

	public:
		Counter();
		~Counter();
		std::string mostCommon();
		void platformCounter(VideoGames*& videoGame);
	};
}